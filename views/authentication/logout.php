<?php

include_once('../../vendor/autoload.php');

use App\user\Authentication;
use App\utility\Utility;
use App\Message\Message;

$auth = new Authentication();


$userStatus = $auth->logOut();


if ($userStatus) {
    // user log out
    // So take him to sign in page

    Message::message("You are successfully logged-out");
    return Utility::redirect('../../index.php');
}
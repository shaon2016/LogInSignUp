<?php

include_once('../../vendor/autoload.php');

use App\user\Authentication;
use App\utility\Utility;
use App\Message\Message;

$auth = new Authentication();


$auth->prepare($_POST);
$userStatus = $auth->isUserValidToLogIn();


if ($userStatus) {
    // user existed
    // So log him to main page
    $_SESSION['user_activation'] = $_POST['email'];
    Utility::redirect("../welcome.php");
} else {
    // not existed
    // Take him to the sign up page
    Message::message("Wrong username or password");
    Utility::redirect("../../index.php");
}

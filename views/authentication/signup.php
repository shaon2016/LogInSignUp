<?php

include_once('../../vendor/autoload.php');

use App\user\User;
use App\user\Authentication;
use App\utility\Utility;
use App\Message\Message;

$auth = new Authentication();
$auth->prepare($_POST);

$userStatus = $auth->isUserExisted();

if (!$userStatus) {
    $user = new User();
    $user->prepare($_POST);
    $user->insertUserDataIntoDatabase();
}else {
    Message::message("This email is occupied!!! Try another email");
    return Utility::redirect('../../index.php');
}


<?php
/**
 * Created by PhpStorm.
 * User: Shaon
 * Date: 7/24/2016
 * Time: 12:49 AM
 */


namespace App\user;

if (!isset($_SESSION['user_activation']))
    session_start();


require('../../vendor/autoload.php');
use App\Model\Database as DB;

class Authentication extends DB
{

    private $EMAIL = "";
    private $PASSWORD = "";

    function __construct()
    {
        parent::__construct();
    }

    public function prepare($data)
    {
        if (array_key_exists('email', $data))
            $this->EMAIL = $data['email'];

        if (array_key_exists('password', $data))
            $this->PASSWORD = $data['password'];

    }

    function isLoggedIn()
    {
        if (array_key_exists('user_activation', $_SESSION) &&
            !empty($_SESSION['user_activation'])
        )
            return true;
        else false;

    }

    function isUserExisted()
    {
        $sql = "SELECT `email` FROM `users` WHERE email = '" . $this->EMAIL . "'";

        $result = mysqli_query($this->DB_CONNECTION, $sql);

        if (mysqli_num_rows($result) > 0) {
            return true;
        } else {
            return false;
        }
    }

    function isUserValidToLogIn()
    {
        $sql = "SELECT `email` FROM `users` WHERE email = '" . $this->EMAIL . "'
            AND password = '" . $this->PASSWORD . "'";

        $result = mysqli_query($this->DB_CONNECTION, $sql);

        if (mysqli_num_rows($result) > 0) {
            return true;
        } else {
            return false;
        }
    }

    function logOut()
    {
        $_SESSION['user_activation'] = "";
        return true;
    }
}
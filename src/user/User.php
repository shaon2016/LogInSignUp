<?php
/**
 * Created by PhpStorm.
 * User: Shaon
 * Date: 7/24/2016
 * Time: 12:48 AM
 */

namespace App\user;
require('../../vendor/autoload.php');
use App\Message\Message;
use App\Model\Database as DB;
use App\utility\Utility;


class User extends DB
{
    private $ID;
    private $FIRST_NAME;
    private $LAST_NAME;
    private $EMAIL;
    private $PASSWORD;

    function __construct()
    {
        parent::__construct();
    }

    function prepare($data)
    {
        if (array_key_exists('id', $data))
            $this->ID = $data['id'];

        if (array_key_exists('firstName', $data))
            $this->FIRST_NAME = $data['firstName'];

        if (array_key_exists('lastName', $data))
            $this->LAST_NAME = $data['lastName'];

        if (array_key_exists('email', $data))
            $this->EMAIL = $data['email'];

        if (array_key_exists('password', $data))
            $this->PASSWORD = $data['password'];

    }

    function insertUserDataIntoDatabase()
    {
        $sql = "INSERT INTO `users` ( `firstName`, `lastName`, `email`, `password`) 
        VALUES ( '" . $this->FIRST_NAME . "', '" . $this->LAST_NAME . "', 
              '" . $this->EMAIL . "', '" . $this->PASSWORD . "')";

        $result = mysqli_query($this->DB_CONNECTION, $sql);

        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Sign Up Successfully.
                </div>");
            Utility::redirect("../../index.php");
        } else {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Sign Up is not Successfully.
                </div>");
            Utility::redirect("../../index.php");
        }


    }
    
}